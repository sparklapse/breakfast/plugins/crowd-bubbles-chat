import type { PluginModule } from "@sparklapse/breakfast";

// Import your components here
import * as Chat from "./components/Chat";

// Define your plugin here
export const id = "crowd-chat";
export const label = "Crowd Chat";
export const version = "1.0.0";
export const author = "dotmrjosh";
export const components: PluginModule["components"] = [
  {
    id: "crowd-chat",
    label: "Crowd Chat",
    component: Chat.Component,
    editor: Chat.Editor,
  },
];
