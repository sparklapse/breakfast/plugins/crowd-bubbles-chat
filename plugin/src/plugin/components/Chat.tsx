import tmi from "tmi.js";
import { For, createUniqueId, onCleanup, onMount } from "solid-js";
import { createStore, produce } from "solid-js/store";
import { TransitionGroup } from "solid-transition-group";
import type {
  ComponentEditorProps,
  ComponentProps,
} from "@sparklapse/breakfast";

type Props = {
  fontSize: number;
  characterLimit: number;
  messageWidth: number;
  messageHeight: number;
};

const DEFAULT_FONT_SIZE = 42;
const DEFAULT_CHARACTER_LIMIT = 80;
const DEFAULT_MESSAGE_WIDTH = 250;
const DEFAULT_MESSAGE_HEIGHT = 150;

const ARROW_SIZE = 40;

export function Editor({ data, setData }: ComponentEditorProps<Props>) {
  return (
    <>
      <p>Message Options</p>
      <div class="flex gap-2">
        <div class="flex flex-col w-full">
          <label>Font Size</label>
          <input
            type="number"
            value={data.fontSize ?? DEFAULT_FONT_SIZE}
            oninput={(ev) => {
              const parsed = parseInt(ev.currentTarget.value);
              if (Number.isNaN(parsed)) return;
              setData("fontSize", parsed);
            }}
          />
        </div>
        <div class="flex flex-col w-full">
          <label>Character Limit</label>
          <input
            type="number"
            value={data.characterLimit ?? DEFAULT_CHARACTER_LIMIT}
            oninput={(ev) => {
              const parsed = parseInt(ev.currentTarget.value);
              if (Number.isNaN(parsed)) return;
              setData("characterLimit", parsed);
            }}
          />
        </div>
      </div>
      <div class="flex gap-2">
        <div class="flex flex-col w-full">
          <label>Width</label>
          <input
            type="number"
            value={data.messageWidth ?? DEFAULT_MESSAGE_WIDTH}
            oninput={(ev) => {
              const parsed = parseInt(ev.currentTarget.value);
              if (Number.isNaN(parsed)) return;

              setData("messageWidth", parsed);
            }}
          />
        </div>
        <div class="flex flex-col w-full">
          <label>Height</label>
          <input
            type="number"
            value={data.messageHeight ?? DEFAULT_MESSAGE_HEIGHT}
            oninput={(ev) => {
              const parsed = parseInt(ev.currentTarget.value);
              if (Number.isNaN(parsed)) return;

              setData("messageHeight", parsed);
            }}
          />
        </div>
      </div>
    </>
  );
}

function Message(props: {
  id: string;
  x: number;
  y: number;
  width: number;
  height: number;
  message: string;
  arrowPos: number;
  arrowSlant?: number;
}) {
  const arrowPosition = () =>
    Math.min(Math.max(0, props.arrowPos), props.width - ARROW_SIZE);
  const arrowSlant = () =>
    [
      props.arrowSlant ?? 50,
      ((props.arrowSlant ?? 50) - 50) * 0.85 + 50,
    ] as const;

  return (
    <div
      class="absolute"
      style={{
        top: `${props.y}px`,
        left: `${props.x}px`,
        background: "white",
        width: `${props.width}px`,
        height: `${props.height}px`,
        "border-radius": "0.5rem",
        border: "4px solid black",
        filter: "drop-shadow(0 0 7px rgba(0, 0, 0, 0.1))",
      }}
    >
      <p
        class="absolute inset-0 p-2"
        style={{
          overflow: "hidden",
          "text-overflow": "ellipsis",
        }}
      >
        {props.message}
      </p>
      <div
        class="absolute"
        style={{
          width: `${ARROW_SIZE}px`,
          height: `${ARROW_SIZE}px`,
          bottom: "0px",
          left: `${arrowPosition()}px`,
          transform: "tanslateY(100%)",
        }}
      >
        <div
          class="absolute"
          style={{
            background: "black",
            transform: "translateY(100%)",
            width: `${ARROW_SIZE}px`,
            height: `${ARROW_SIZE}px`,
            "clip-path": `polygon(0% 0%, 50% 10%, 100% 0%, ${
              arrowSlant()[0]
            }% 100%)`,
          }}
        />
        <div
          class="absolute"
          style={{
            background: "white",
            transform: "translateY(100%)",
            width: `${ARROW_SIZE}px`,
            height: `${ARROW_SIZE}px`,
            "clip-path": `polygon(10% 0%, 90% 0%, ${arrowSlant()[1]}% 80%)`,
          }}
        />
      </div>
    </div>
  );
}

export function Component({ data, transform }: ComponentProps<Props>) {
  const [messages, setMessages] = createStore<Parameters<typeof Message>[0][]>(
    []
  );

  const msgWidth = () => data.messageWidth ?? DEFAULT_MESSAGE_WIDTH;
  const msgHeight = () => data.messageHeight ?? DEFAULT_MESSAGE_HEIGHT;

  const client = tmi.Client({
    channels: ["REPLACE-ME"],
  });

  let xShift = 0;
  client.addListener("message", (_channel, _tags, message) => {
    if (message.length > (data.characterLimit ?? DEFAULT_CHARACTER_LIMIT))
      return;

    xShift += msgWidth() + Math.floor(Math.random() * msgWidth());
    xShift %= transform.width - msgWidth();
    const newMsg = {
      id: createUniqueId(),
      x: xShift,
      y: Math.floor(
        Math.random() * Math.max(0, transform.height - msgHeight() - ARROW_SIZE)
      ),
      width: msgWidth(),
      height: msgHeight(),
      message,
      arrowPos: Math.floor(Math.random() * (msgWidth() - 100)),
      arrowSlant: Math.floor(Math.random() * 100),
    };

    setMessages(
      produce((prev) => {
        if (prev.length > 5) prev.shift();
        prev.push(newMsg);
      })
    );
    setTimeout(() => {
      const idx = messages.findIndex((v) => v.id === newMsg.id);
      if (idx >= 0)
        setMessages(
          produce((prev) => {
            prev.splice(idx, 1);
          })
        );
    }, 3000);
  });

  onMount(() => {
    client.connect();
  });

  onCleanup(() => {
    client.disconnect();
    client.removeAllListeners();
  });

  return (
    <div
      class="absolute inset-0 flex gap-2"
      style={{ "font-size": `${data.fontSize ?? DEFAULT_FONT_SIZE}px` }}
    >
      <TransitionGroup
        onEnter={(el, done) => {
          const a = el.animate(
            [
              { opacity: 0, transform: "translateY(50px)" },
              { opacity: 1, transform: "translateY(0)" },
            ],
            {
              duration: 100,
              easing: "cubic-bezier(0, 0.5, 0.5, 1)",
            }
          );
          a.finished.then(done);
        }}
        onExit={(el, done) => {
          const a = el.animate([{ opacity: 1 }, { opacity: 0 }], {
            duration: 100,
          });
          a.finished.then(done);
        }}
      >
        <For each={messages}>
          {(item) => (
            <Message
              id={item.id}
              x={item.x}
              y={item.y}
              message={item.message}
              width={item.width}
              height={item.height}
              arrowPos={item.arrowPos}
              arrowSlant={item.arrowSlant}
            />
          )}
        </For>
      </TransitionGroup>
    </div>
  );
}
